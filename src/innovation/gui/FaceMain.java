package innovation.gui;

import  org.opencv.core.Core;

import javafx.scene.image.Image;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.fxml.FXMLLoader;
import innovation.gui.FaceDetectionController;


/**
 * The main class for a JavaFX application. It create the windows of application
 * @author <a href="ibrahima.mbaye@sogeti.com">Ibrahima mbaye </a>
 * @version 1.0 (2018-06-20)
 *
 */

public class FaceMain extends Application {

    @Override
    public void start(Stage primaryStage){
        try {
                 // lOAD THE FXML file
                //Parent resourceLoader = FXMLLoader.load(getClass().getResource("Template.fxml"));
                FXMLLoader resourceLoader = new  FXMLLoader(getClass().getResource("Template.fxml"));

                AnchorPane rootPanel =  resourceLoader.load();
                FaceDetectionController faceDetectionController = resourceLoader.getController();
                faceDetectionController.setMain(this);
                Scene scene = new Scene(rootPanel, 800, 600);
                // create  the scene containing the root layout.
                primaryStage.getIcons().add(new Image("innovation/asset/logo_2.jpg"));
                primaryStage.setTitle("YVE - Face DETECT - Proof of concept");
                primaryStage.setScene(scene);
                // show the interface
                primaryStage.show();


                // init the controller
                FaceDetectionController controllerFaceDetector = resourceLoader.getController();
                controllerFaceDetector.init();


                primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                    @Override
                    public void handle(WindowEvent event) {
                        controllerFaceDetector.setClosed();
                    }
                });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    /*
     * For launching the application....
     * @param args
     * optional params
     * */
    public static void main(String[] args) {
       System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        launch(args);
    }




}
