package innovation.gui;

import java.util.List;

/*
   This application handles a video stream and try to find any possible human face in a frame.
   @author <a href="ibrahima.mbaye@sogeti.com">Ibrahima mbaye </a>
 * @author <a href="salem.bara@sogeti.com">Salem Bara </a>
 * @author <a href="laura.morel@sogeti.com">laura Morel</a>
 * @author <a href="vincent.sebastien@sogeti.com">Vincent Sebastien </a>
 *
 * @version 1.0 (2018-06-20)
 */

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import innovation.utils.Utils;
import javafx.scene.image.Image;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;
import org.opencv.videoio.VideoCapture;
import org.opencv.core.Size;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.image.ImageView;

public class FaceDetectionController {

	// FXML BUTTONS OFF and ON
	@FXML
	private Button cameraButtonOn;
	// @FXML private Button cameraButtonOff;
	@FXML
	private CheckBox eyesDetect;
	@FXML
	private CheckBox mouthDetect;
	@FXML
	private CheckBox faceDetect;
	@FXML
	private CheckBox detectAll;

	@FXML
	private ImageView currentFrame;

	// video capture
	private VideoCapture capture = new VideoCapture();
	private boolean cameraActive = false;
	private FaceMain main;

	// The id of the camera to be used
	private static int cameraId = 0;
	private CascadeClassifier faceCascade;
	private ScheduledExecutorService timer;
	// private int absoluteFaceSize;

	void init() {
		// this.absoluteFaceSize = 0;
		this.capture = new VideoCapture();
		this.faceCascade = new CascadeClassifier();
		currentFrame.setFitWidth(800);
		currentFrame.setPreserveRatio(true);
	}

	// private Button getCameraButtonOff() {
	// return cameraButtonOff;
	// }

	private Button getCameraButtonOn() {
		return cameraButtonOn;
	}

	// connect main class to controller
	void setMain(FaceMain main) {
		this.main = main;
	}

	@FXML
	protected void startCamera() {
		// TODO Function to start Camera
		if (!this.cameraActive) {
			this.capture.open(cameraId);
			if (this.capture.isOpened()) {
				this.cameraActive = true;

				this.eyesDetect.setDisable(true);
				this.faceDetect.setDisable(true);
				this.mouthDetect.setDisable(true);
				this.detectAll.setDisable(true);

				Runnable frameCatcher = () -> {
					Mat frame = grabFrame();
					Image imageToShow = Utils.mat2Image(frame);
					updateImageView(currentFrame, imageToShow);
				};
				// this.getCameraButtonOff().setDisable(false);
				this.timer = Executors.newSingleThreadScheduledExecutor();
				this.timer.scheduleAtFixedRate(frameCatcher, 0, 30, TimeUnit.MILLISECONDS);
			} else {
				// log the error
				System.err.println("Failed Open camera");
			}
		} else {
			this.eyesDetect.setDisable(false);
			this.faceDetect.setDisable(false);
			this.mouthDetect.setDisable(false);
			this.detectAll.setDisable(false);

			this.cameraActive = false;
			this.mouthDetect.setDisable(false);
			this.eyesDetect.setDisable(false);
			this.faceDetect.setDisable(false);
			this.detectAll.setDisable(false);
			this.stopCamera();
		}
		System.out.println("hit button start camera");

		if (("Start camera").equals(this.cameraButtonOn.getText())) {

			this.cameraButtonOn.setText("Stop Camera");
		} else
			this.cameraButtonOn.setText("Start camera");

	}

	private void updateImageView(ImageView currentFrame, Image imageToShow) {
		Utils.onFXThread(currentFrame.imageProperty(), imageToShow);
	}

	private Mat grabFrame() {
		Mat frame = new Mat();
		if (this.capture.isOpened()) {
			try {
				this.capture.read(frame);
				if (!frame.empty()) {
					this.detectAndDisplay(frame);
				}

			} catch (Exception e) {
				System.err.println("Exception" + e);
			}
		}

		return frame;
	}

	private void detectAndDisplay(Mat frame) {
		System.out.println("\nRunning DetectFaceDemo");

		MatOfRect detect = new MatOfRect();
		Mat grayFrame = new Mat();

		// convert the frame in gray scale
		Imgproc.cvtColor(frame, grayFrame, Imgproc.COLOR_BGR2GRAY);
		// equalize the frame histogram to improve the result
		Imgproc.equalizeHist(grayFrame, grayFrame);

		// compute minimum face size (20% of the frame height, in our case)

		// System.out.println(" display absolute face size "+ this.absoluteFaceSize);

		faceCascade.detectMultiScale(grayFrame, detect);

		List<Rect> listOfFaces = detect.toList();
		for (Rect face : listOfFaces) {
			Point center = new Point(face.x + face.width / 2, face.y + (face.height / 2));
			Imgproc.ellipse(frame, center, new Size(face.width / 2, face.height / 2), 0, 0, 360, new Scalar(0, 255, 0),
					2);
		}

		MatOfRect detect2 = new MatOfRect();

		List<Rect> listOfOthers = detect.toList();
		for (Rect face2 : listOfOthers) {
			Point center = new Point(face2.x + face2.width / 2, face2.y + (face2.height / 2));
			Imgproc.ellipse(frame, center, new Size(face2.width / 2, face2.height / 2), 0, 0, 360,
					new Scalar(0, 255, 0), 2);
		}

	}

	@FXML
	private void stopCamera() {
		// TODO function for stop camera
		System.out.println("hit button stop camera");

		if (this.timer != null && !this.timer.isShutdown()) {
			try {
				// stop the timer
				this.timer.shutdown();
				this.timer.awaitTermination(20, TimeUnit.MILLISECONDS);
				// this.getCameraButtonOn().setDisable(true);
			} catch (InterruptedException e) {
				// log any exception
				System.err.println("Exception in stopping the frame capture, trying to release the camera now... " + e);
			}
		}

		if (this.capture.isOpened()) {
			// release the camera
			this.capture.release();
		}

	}

	@FXML
	private void checkboxSelection(String filename) {
				// load the classifier(s)

		this.faceCascade.load(filename);

	}

	@FXML
	protected void eyeSelected(Event event) {

		if (this.eyesDetect.isSelected()) {
			this.eyesDetect.setSelected(true);
			this.detectAll.setSelected(false);
			this.faceDetect.setSelected(false);
			this.mouthDetect.setSelected(false);
		}

		this.checkboxSelection("resources/haarcascade_eye_tree_eyeglasses.xml");
	}

	@FXML
	protected void faceSelected(Event event) {
		if (this.faceDetect.isSelected()) {
			this.faceDetect.setSelected(true);
			this.detectAll.setSelected(false);
			this.eyesDetect.setSelected(false);
			this.mouthDetect.setSelected(false);
		}

		this.checkboxSelection("resources/haarcascade_frontalface_alt2.xml");

	}

	@FXML
	protected void mouthSelected(Event event) {

		if (this.mouthDetect.isSelected()) {
			this.mouthDetect.setSelected(true);
			this.detectAll.setSelected(false);
			this.eyesDetect.setSelected(false);
			this.faceDetect.setSelected(false);
			this.checkboxSelection("resources/Mouth.xml");
		}

	}

	@FXML
	protected void detectAll(Event event) {
		// TODO create the function of detect mouth , eyes, face
		if (this.detectAll.isSelected()) {
			this.detectAll.setSelected(true);
			this.faceDetect.setSelected(true);
			this.eyesDetect.setSelected(true);
			this.mouthDetect.setSelected(true);

			this.checkboxSelection("resources/profileface.xml");


		}

	}

	void setClosed() {
		this.stopCamera();
	}
}
